

#ifndef BAYESEXAMPLE_DATASET_H
#define BAYESEXAMPLE_DATASET_H


#include <vector>
#include <string>
#include <regex>

class DataSet {
public:
    DataSet(unsigned short age, unsigned short income, bool study, unsigned short creditRating);
    void bayesClassifier(std::string trainingDataSetFile);
    unsigned short bayesClassifierForTrainer(std::string trainingDataSetFile);
private:
    unsigned short age;
    unsigned short income;
    bool study;
    unsigned short creditRating;

    std::string currentLine;
    std::vector<unsigned short> ageSet;
    std::vector<unsigned short> incomeSet;
    std::vector<bool> studySet;
    std::vector<unsigned short> creditRatingSet;
    std::vector<bool> finalDecisionSet; //If someone has bought a PC - true, otherwise false
    std::vector<float> probs;

    void calculateProb(float data, bool decision, unsigned short dataColumn);
    std::vector<std::string> split(const std::string& input, const std::string& regex) {
        // passing -1 as the submatch index parameter performs splitting
        std::regex re(regex);
        std::sregex_token_iterator
                first{input.begin(), input.end(), re, -1},
                last;
        return {first, last};
    }
};


#endif //BAYESEXAMPLE_DATASET_H
