#include <iostream>

int main() {
    int tablica[] = {5, 1, 4, 8, 9, 9, 1, 0, -10, 15, 7}; //poczatkowa, nieposortowana tablica
    constexpr size_t iloscElementow = sizeof(tablica) / sizeof(*tablica);
    std::qsort(tablica, iloscElementow, sizeof(*tablica), [](const void* a, const void* b)
    {
        int arg1 = *static_cast<const int*>(a);
        int arg2 = *static_cast<const int*>(b);
        return arg1 < arg2 ? -1 : arg1 > arg2 ? 1 : 0;
    });

    for (int a : tablica) std::cout << a << " ";
    std::cout << std::endl;

    /* -10 0 1 1 4 5 7 8 9 9 15 (po sortowaniu)*/

    return 0;
}