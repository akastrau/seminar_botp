//
// Created by adrian on 12/8/16.
//

#ifndef BAYESEXAMPLE_TRAINER_H
#define BAYESEXAMPLE_TRAINER_H


#include <vector>
#include <regex>
#include <random>

class Trainer {
public:
    Trainer();
    static void train(const std::string fileName);

};


#endif //BAYESEXAMPLE_TRAINER_H
