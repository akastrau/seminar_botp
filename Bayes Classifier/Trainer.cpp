//
// Created by adrian on 12/8/16.
//

#include <iostream>
#include <fstream>
#include "Trainer.h"
#include "DataSet.h"

void Trainer::train(std::string fileName) {

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> distribution(20, 50);
    std::uniform_int_distribution<int> boolDistribution(0, 1);
    std::uniform_int_distribution<int> distribution1(0,2);

    for (int i = 0; i < 200; ++i) {
        int age = distribution(mt);
        int income = distribution1(mt);
        bool study = boolDistribution(mt);
        int creditRating = distribution1(mt);
        DataSet person(age, income, study, creditRating);

        int decision = person.bayesClassifierForTrainer(fileName);
        if (decision == 2) continue;

        std::ofstream outputFile(fileName, std::ios::app);
        outputFile << "\n" << age << "," << income
                                         << ","
                                         << study
                                         << ","
                                         << creditRating
                                         << ","
                                         << decision;
        outputFile.close();
    }
}

Trainer::Trainer() {

}
