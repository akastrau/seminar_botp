#include <iostream>
#include "DataSet.h"
#include "Trainer.h"

void welcomeMsg(){
    std::cout << "Seminarium PFT - 9.12.2016" << "\nAdrian Kastrau, Szymon Kamiński\nBayes Classifier\n\n";
}

int main(int argc, char** argv) {
    welcomeMsg();
    if (argc == 2 && argv[1] == std::string("-t")){
        std::cout << "Training... " << std::flush;
        Trainer::train("trainingSet.txt");
        std::cout << "[OK]\n\n";
        return 0;
    }
    else if (argc == 1){
        std::cout << "\n\tEntering normal mode...\n" << std::flush;

        DataSet person(37,2,1,2);
        person.bayesClassifier("trainingSet.txt");

        return 0;
    }
    else {
        std::cout << "Usage: " << argv[0] << " -t " << "\tTrain using current training set\n";
        std::cout << argv[0] << "\tRun program in normal mode\n\n";
        return 1;
    }
}
