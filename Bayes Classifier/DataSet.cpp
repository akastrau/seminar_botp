
#include <fstream>
#include <iostream>
#include <sstream>
#include "DataSet.h"

DataSet::DataSet(unsigned short age, unsigned short income, bool study, unsigned short creditRating) {
    this->age = age;
    this->income = income;
    this->study = study;
    this->creditRating = creditRating;
}

void DataSet::bayesClassifier(std::string trainingDataSetFile) {
    std::ifstream dataSetFile;
    std::stringstream stringStream;
    stringStream << age << "," << income << "," << study << "," << creditRating;
    std::string currentPerson = stringStream.str();
    try {
        std::cout << "Reading training set... ";
        size_t readCounter = 0;
        dataSetFile.open(trainingDataSetFile);
        while (getline(dataSetFile, currentLine)){
            ++readCounter;
            if (currentLine.find(currentPerson) != std::string::npos){
                std::cout << "[WARNING]\n\n\t" << "Current person is in the training set!\n";
                std::vector<std::string> parsedData = split(currentLine, ",");
                if (std::stoi(parsedData[4]) == true) std::cout << "\n\tThis person has bought a new PC!\n";
                else std::cout << "\n\tThis person hasn't bought a new PC!\n";
                dataSetFile.close();
                return;
            }
            std::vector<std::string> parsedData = split(currentLine, ",");
            ageSet.push_back(std::stof(parsedData[0]));
            incomeSet.push_back(std::stoi(parsedData[1]));
            studySet.push_back(std::stoi(parsedData[2]));
            creditRatingSet.push_back(std::stoi(parsedData[3]));
            finalDecisionSet.push_back(std::stoi(parsedData[4]));
            parsedData.clear();
            currentLine.clear();
        }
        dataSetFile.close();
        if (readCounter == 0){
            std::cout << "[FAIL] " << std::endl;
            exit(1);
        }
        std::cout << "[OK]" << std::endl;
    }
    catch (std::exception& exception){
        std::cout << exception.what() << std::endl;
    }

    std::cout << "Calculating...";
    calculateProb(age, true, 0);
    calculateProb(income, true, 1);
    calculateProb(study, true, 2);
    calculateProb(creditRating, true, 3);

    float probYes = 1.0f;
    float probNo = 1.0f;

    for (float a : probs)
        probYes *= a;
    probs.clear();

    calculateProb(age, false, 0);
    calculateProb(income, false, 1);
    calculateProb(study, false, 2);
    calculateProb(creditRating, false, 3);

    for (float a : probs)
        probNo *= a;
    probs.clear();

    size_t trueCounter = 0; //How many computers has been bought
    size_t falseCounter = 0;

    for (auto a : finalDecisionSet)
        if (a == true)
            ++trueCounter;
    falseCounter = finalDecisionSet.size() - trueCounter;

    probYes *= ((float)trueCounter / finalDecisionSet.size());
    probNo *= ((float)falseCounter / finalDecisionSet.size());

    std::cout << "[DONE]";

    std::cout << "\n\tStatistics:\n\tChance that human has bought a new PC: " << probYes;
    std::cout << "\n\tChance that human hasn't bought a new PC: " << probNo << "\n\n";

    if (probYes > probNo) std::cout << "\t\tHuman has bought a new PC... yaay :)" << "\n";
    else std::cout << "\t\tHuman hasn't bought a new PC... so sad :(" << "\n";

}

void DataSet::calculateProb(float data, bool decision, unsigned short dataColumn) {
    size_t size = ageSet.size();
    size_t counter = 0; //How much true statements in the last column
    size_t howManyinClass = 0;

    for (int i = 0; i < size; ++i) {
        if (finalDecisionSet[i] == true) ++counter;
    }

    switch (dataColumn) {
        case 0:
            for (int i = 0; i < size; ++i) {
                if (data == ageSet[i] && finalDecisionSet[i] == decision) ++howManyinClass;
            }
            if (howManyinClass != 0 && decision == true) probs.push_back((float)howManyinClass / counter);
            if (howManyinClass != 0 && decision == false) probs.push_back((float)howManyinClass / (size - counter));
            howManyinClass = 0;
            break;
        case 1:
            for (int i = 0; i < size; ++i) {
                if (data == incomeSet[i] && finalDecisionSet[i] == decision) ++howManyinClass;
            }
            if (howManyinClass != 0 && decision == true) probs.push_back((float)howManyinClass / counter);
            if (howManyinClass != 0 && decision == false) probs.push_back((float)howManyinClass / (size - counter));
            howManyinClass = 0;
            break;
        case 2:
            for (int i = 0; i < size; ++i) {
                if (data == studySet[i] && finalDecisionSet[i] == decision) ++howManyinClass;
            }
            if (howManyinClass != 0 && decision == true) probs.push_back((float)howManyinClass / counter);
            if (howManyinClass != 0 && decision == false) probs.push_back((float)howManyinClass / (size - counter));
            howManyinClass = 0;
            break;
        case 3:
            for (int i = 0; i < size; ++i) {
                if (data == creditRatingSet[i] && finalDecisionSet[i] == decision) ++howManyinClass;
            }
            if (howManyinClass != 0 && decision == true) probs.push_back((float)howManyinClass / counter);
            if (howManyinClass != 0 && decision == false) probs.push_back((float)howManyinClass / (size - counter));
            howManyinClass = 0;
            break;
    }
}

unsigned short DataSet::bayesClassifierForTrainer(std::string trainingDataSetFile) {
    std::ifstream dataSetFile;
    std::stringstream stringStream;
    stringStream << age << "," << income << "," << study << "," << creditRating;
    std::string currentPerson = stringStream.str();
    try {
        size_t readCounter = 0;
        dataSetFile.open(trainingDataSetFile);
        while (getline(dataSetFile, currentLine)) {
            ++readCounter;
            if (currentLine.find(currentPerson) != std::string::npos){
                std::vector<std::string> parsedData = split(currentLine, ",");
                dataSetFile.close();
                return 2;
            }
            std::vector<std::string> parsedData = split(currentLine, ",");
            ageSet.push_back(std::stof(parsedData[0]));
            incomeSet.push_back(std::stoi(parsedData[1]));
            studySet.push_back(std::stoi(parsedData[2]));
            creditRatingSet.push_back(std::stoi(parsedData[3]));
            finalDecisionSet.push_back(std::stoi(parsedData[4]));
            parsedData.clear();
            currentLine.clear();
        }
        dataSetFile.close();
        if (readCounter == 0) {
            std::cout << "Input/output error!\n";
            exit(1);
        }
    }
    catch (std::exception &exception) {
        std::cout << exception.what() << std::endl;
    }

    calculateProb(age, true, 0);
    calculateProb(income, true, 1);
    calculateProb(study, true, 2);
    calculateProb(creditRating, true, 3);

    float probYes = 1.0f;
    float probNo = 1.0f;

    for (float a : probs)
        probYes *= a;
    probs.clear();

    calculateProb(age, false, 0);
    calculateProb(income, false, 1);
    calculateProb(study, false, 2);
    calculateProb(creditRating, false, 3);

    for (float a : probs)
        probNo *= a;
    probs.clear();

    size_t trueCounter = 0; //How many computers has been bought
    size_t falseCounter = 0;

    for (auto a : finalDecisionSet)
        if (a == true)
            ++trueCounter;
    falseCounter = finalDecisionSet.size() - trueCounter;

    probYes *= ((float) trueCounter / finalDecisionSet.size());
    probNo *= ((float) falseCounter / finalDecisionSet.size());

    if (probYes > probNo) return true;
    else return false;
}
